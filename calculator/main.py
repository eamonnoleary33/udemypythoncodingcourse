from art import logo
import os

def add(n1, n2):
  return n1 + n2

def subtract(n1, n2):
  return n1 - n2

def multiply(n1, n2):
  return n1 * n2

def divide(n1, n2):
  return n1 / n2

operations = {
  "+" : add,
  "-" : subtract,
  "*" : multiply,
  "/" : divide
}

def calculator():
  print(logo)
  num1 = float(input("What is the first number?: "))
  num2 = float(input("What is the second number?: "))
  
  for key in operations:
    print(key)
  should_continue = True

  while should_continue:
    operation = input("Pick an operation: ")
    
    calculation_function = operations[operation]
    answer = calculation_function(num1, num2)
    
    print(f"{num1} {operation} {num2} = {answer}")
  
    if input(f"Type 'y' to continue calculating with {answer}, or type 'n' to start a new calculation: ") == 'y':
      num1 = answer
    else:
      should_continue = False
      os.system('clear')
      calculator()

calculator()