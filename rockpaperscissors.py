import random, sys

rock = '''
    _______
---'   ____)
      (_____)
      (_____)
      (____)
---.__(___)
'''

paper = '''
    _______
---'   ____)____
          ______)
          _______)
         _______)
---.__________)
'''

scissors = '''
    _______
---'   ____)____
          ______)
       __________)
      (____)
---.__(___)
'''

game_images = [rock, paper, scissors]
print("Welcome to Rock Paper Scissors\nEnter 0 for Rock\nEnter 1 for Paper\nEnter 2 for Scissors")
choice = int(input("Your choice: "))
if choice < 0 or choice > 2:
  print("Invalid choice, must be either 0, 1, 2")
  sys.exit()
else:
  print(game_images[choice])

computer_choice = random.randint(0,2)
print(f"Computer choice:\n{game_images[computer_choice]}")

if choice == computer_choice:
  print("Draw")
elif choice == 0 and computer_choice == 2:
  print("You win")
elif choice == 1 and computer_choice == 0:
  print("You win")
elif choice == 2 and computer_choice == 1:
  print("You win")
else:
  print("You lose")
