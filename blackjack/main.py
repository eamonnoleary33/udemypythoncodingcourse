import random
import os
from art import logo

print(logo)

def deal_card():
    # Select a random card from the standard deck
    cards = [11, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10]
    card = random.choice(cards)
    return card

def calculate_score(list_of_cards):
    # Calculate the total score of a hand, handle the presence of an Ace
    score = sum(list_of_cards)
    if score == 21 and len(list_of_cards) == 2:
        return 0  # Blackjack
    elif 11 in list_of_cards and score > 21:
        list_of_cards.remove(11)
        list_of_cards.append(1)
        score = sum(list_of_cards)  # Convert Ace from 11 to 1
    return score

def compare(user_score, computer_score):
    # Compare the final scores and determine the winner or a draw
    if user_score == computer_score:
        print("Draw")
    elif user_score == 0:
        print("User wins with a blackjack!")
    elif computer_score == 0:
        print("Computer wins with a blackjack!")
    elif user_score > 21:
        print("Computer wins. You went over 21.")
    elif computer_score > 21:
        print("User wins. Computer went over 21.")
    elif user_score > computer_score:
        print("User wins!")
    else:
        print("Computer wins!")

while True:
    is_game_over = False
    user_cards = []
    computer_cards = []

    # Deal initial cards to the user and the computer
    for _ in range(2):
        user_cards.append(deal_card())
        computer_cards.append(deal_card())

    user_score = calculate_score(user_cards)
    computer_score = calculate_score(computer_cards)

    print(f"Your cards: {user_cards}, current score: {user_score}")
    print(f"Computer first card: {computer_cards[0]} ")

    if user_score == 0 or computer_score == 0 or user_score > 21:
        is_game_over = True

    # Allow the user to draw additional cards
    while user_score < 21 and not is_game_over:
        continue_game = input("Would you like to draw another card? Enter yes or no: ")
        if continue_game == "yes":
            user_cards.append(deal_card())
            user_score = calculate_score(user_cards)
            print(f"Your cards: {user_cards}, current score: {user_score}")
        else:
            break

    # Let the computer draw cards until it reaches a score of 17 or higher
    while computer_score < 17 and not is_game_over:
        computer_cards.append(deal_card())
        computer_score = calculate_score(computer_cards)

    print(f"User Score: {user_score}")
    print(f"Computer Score: {computer_score}")

    # Compare final scores and determine the winner
    compare(user_score, computer_score)
    print(f"Your final hand: {user_cards}, final score: {user_score}")
    print(f"Computer's final hand: {computer_cards}, final score: {computer_score}")

    # Ask the user if they want to play again
    play_again = input("Do you want to play again? Enter yes or no: ").lower()
    if play_again == "yes":
        os.system('clear')  # Clear the console for a new game
        print(logo)
    if play_again != "yes":
        break  # Exit the loop and end the game
