from art import logo
from game_data import data
import random, os

def print_scores(current_score, high_score):
    if current_score <= high_score:
        print(f"Current Score: {current_score} | High Score: {high_score}")
    elif current_score > high_score:
        print(f"Current High Score: {current_score} | Old High Score: {high_score}")

def game():
    os.system('clear')
    global high_score  # Declare high_score as global
    options = []
    print(logo)
    option1 = random.choice(data)
    data.remove(option1) # won't reappear, stops user from having to choose between the same person
    options.append(option1)
    option2 = random.choice(data)
    data.remove(option2)
    options.append(option2)
    #print(options) # debugging
    print_scores(score, high_score)  # Print current and high scores
    print("Welcome to higher-lower the goal of the game is to guess who has more Instagram followers")
    print(f"1: {option1['name']}, a {option1['description']}, from {option1['country']}.")
    print(f"2: {option2['name']}, a {option2['description']}, from {option2['country']}.")

    choice = int(input(f"Who has a higher following on Instagram? Enter 1 for {option1['name']}, or 2 for {option2['name']}: "))

    if choice == 1:
        if option1['follower_count'] > option2['follower_count']:
            print("Correct")
            return True
        else:
            print("Incorrect")
            return False
    elif choice == 2:
        if option2['follower_count'] > option1['follower_count']:
            print("Correct")
            return True
        else:
            print("Incorrect")
            return False

def get_high_score():
    try:
        with open('highscore.txt', 'r') as file:
            return int(file.read())
    except FileNotFoundError:
        return 0

def update_high_score(new_score):
    with open('highscore.txt', 'w') as file:
        file.write(str(new_score))

score = 0
high_score = get_high_score()
while game():
    score += 1

print(f"Final score: {score}")

high_score = get_high_score()
if score > high_score:
    print(f"New High Score: {score}!")
    update_high_score(score)
else:
    print(f"High Score: {high_score}")
