import os
from art import logo
os.system('clear')
print(logo)

bidders = []
game_state = True
while game_state:
  name = input("Welcome to the secret auction program.\nWhat is your name?: ")
  bid = int(input("What is your bid?: "))
  contd = input("Are there anymore bidders 'yes' or 'no'?: ")

  bidder_info = {"name" : name, "bid" : bid}
  bidders.append(bidder_info)
  if contd == "yes":
    os.system('clear')
  else:
    game_state = False

max_bidder = max(bidders, key=lambda x: x["bid"])
max_name = max_bidder["name"]
max_bid = max_bidder["bid"]

print(logo)
print(f"The winner is {max_name} with a bid of {max_bid}")
  
