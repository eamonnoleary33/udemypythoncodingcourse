from art import logo, congrats
import random
import os

def game():
    # Generate a random number between 1 and 100 for the player to guess
    correct_num = random.randint(1, 100)
    #print(correct_num)  # Debugging: Print the correct number
    print(logo)
    
    # Get the desired difficulty level from the user
    difficulty = input("Welcome to my Number Guessing game\nYou have to guess a number between 1 to 100\nPlease enter your desired difficulty\nEasy\nHard\nYour Choice: ").lower()
    
    # Set the number of initial guesses based on difficulty level
    guesses = 0
    if difficulty == "easy":
        print("Easy? hahaha, afraid of a challenge?\nYou have been given 10 guesses")
        guesses = 10
    elif difficulty == "hard":
        print("Now it's time to test yourself!\nYou have been given 5 guesses")
        guesses = 5
    
    closeness = 0
    
    # Main game loop where the user guesses the number
    while guesses > 0:
        guess = int(input())
        
        # Check if the guess is correct
        if guess == correct_num:
            print(congrats)
            print("You've guessed correctly")
            guesses = 0
            break
        
        # Calculate closeness to the correct number
        if guess > correct_num:
            closeness = guess - correct_num
        elif correct_num > guess:
            closeness = correct_num - guess

        # Provide feedback based on the closeness
        if closeness in range(1, 5):
            print("You are very close")
        elif closeness in range(6, 20):
            print("You are close")
        else:
            print("You are not close")
        
        # Decrement the remaining guesses
        guesses -= 1

# Main loop to manage multiple game plays and ask if the user wants to play again
while True:
    game()
    restart = input("Do you want to play again? Enter 'yes' or 'no': ").lower()
    
    # Check if the user wants to play again, otherwise exit the program
    if restart != 'yes':
        print("Thanks for playing! Goodbye.")
        break
    else:
        os.system('clear')  # Clear the console for a new game
