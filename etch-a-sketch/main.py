from turtle import Turtle, Screen

tim = Turtle()
screen = Screen()

def move_forwards():
    tim.forward(10)

def move_backwards():
    tim.backward(10)

def move_clockwise():
    tim.right(10)
    tim.forward()
    # new_heading = tim.heading() + 10
    # tim.setheading(new_heading)

def move_counter_clockwise():
    tim.left(10) 
    tim.forward()

def clear_drawing():
    tim.clear()
    tim.reset()

screen.listen()
screen.onkey(key="w", fun=move_forwards)
screen.onkey(key="s", fun=move_backwards)
screen.onkey(key="d", fun=move_clockwise)
screen.onkey(key="a", fun=move_counter_clockwise)
screen.onkey(key="c", fun=clear_drawing)
screen.exitonclick()